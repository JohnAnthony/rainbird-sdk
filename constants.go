package sdk

const (
	EnvCommunity  = "https://api.rainbird.ai"
	EnvEnterprise = "https://api-enterprise.rainbird.ai"
	Version       = "0.1.0"
)
